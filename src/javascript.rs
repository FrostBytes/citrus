use wasm_bindgen::prelude::*;

use cgmath::Matrix3;

#[wasm_bindgen]
extern {
    #[wasm_bindgen]
    pub type HTMLImageElement;

    #[wasm_bindgen(constructor, js_class = Image)]
    pub fn constructor() -> HTMLImageElement;

    #[wasm_bindgen(method, setter, js_name = src)]
    pub fn set_src(this: &HTMLImageElement, src: &str);

    #[wasm_bindgen(method, getter)]
    pub fn complete(this: &HTMLImageElement) -> bool;
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen]
    pub type HTMLCanvasElement;

    #[wasm_bindgen(method, getter)]
    pub fn width(this: &HTMLCanvasElement) -> i32;
    #[wasm_bindgen(method, getter)]
    pub fn height(this: &HTMLCanvasElement) -> i32;
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen]
    pub type CanvasRenderingContext2D;

    #[wasm_bindgen(method, js_name = setTransform)]
    fn set_transform_internal(this: &CanvasRenderingContext2D, a: f32, b: f32, c: f32, d: f32, e: f32, f: f32);
    #[wasm_bindgen(method, js_name = resetTransform)]
    pub fn reset_transform(this: &CanvasRenderingContext2D);

    #[wasm_bindgen(method, js_name = drawImage)]
    pub fn draw_image(this: &CanvasRenderingContext2D, image: &HTMLImageElement, dx: i32, dy: i32, dw: i32, dh: i32);

    #[wasm_bindgen(method, js_name = fillRect)]
    pub fn fill_rect(this: &CanvasRenderingContext2D, dx: i32, dy: i32, dw: i32, dh: i32);
    #[wasm_bindgen(method, js_name = clearRect)]
    pub fn clear_rect(this: &CanvasRenderingContext2D, dx: i32, dy: i32, dw: i32, dh: i32);

    #[wasm_bindgen(method, setter, js_name = fillStyle)]
    pub fn set_fill_color(this: &CanvasRenderingContext2D, color: &str);

    #[wasm_bindgen(method)]
    pub fn save(this: &CanvasRenderingContext2D);
    #[wasm_bindgen(method)]
    pub fn restore(this: &CanvasRenderingContext2D);

    #[wasm_bindgen(method, getter)]
    pub fn canvas(this: &CanvasRenderingContext2D) -> HTMLCanvasElement;
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen]
    pub type MouseEvent;

    #[wasm_bindgen(method, getter)]
    pub fn buttons(this: &MouseEvent) -> u8;

    #[wasm_bindgen(method, getter, js_name = offsetX)]
    pub fn offset_x(this: &MouseEvent) -> f32;
    #[wasm_bindgen(method, getter, js_name = offsetY)]
    pub fn offset_y(this: &MouseEvent) -> f32;
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen(extends = MouseEvent)]
    pub type WheelEvent;

    #[wasm_bindgen(method, getter, js_name = deltaY)]
    pub fn delta_y(this: &WheelEvent) -> f32;
}

impl HTMLImageElement {
    /// Creates a new HTML image with a source.
    pub fn new(src: &str) -> HTMLImageElement {
        let image = HTMLImageElement::constructor();
        image.set_src(src);
        image
    }
}

impl CanvasRenderingContext2D {
    /// Set the transform
    pub fn set_transform(&self, m: Matrix3<f32>) {
        self.set_transform_internal(m.x.x, m.x.y, m.y.x, m.y.y, m.z.x, m.z.y);
    }

    /// Clears the canvas
    pub fn clear(&self) {
        self.save();
        self.reset_transform();
        self.clear_rect(0, 0, self.canvas().width(), self.canvas().height());
        self.restore();
    }
}

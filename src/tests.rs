use crate::field::Field;
use crate::field::tile::TileKind;
use crate::editor::Editor;

use cgmath::Vector2;
use cgmath::prelude::*;

#[test]
fn test_iter() {
    const POS: [(i32, i32); 4] = [(0, 0), (1, 0), (0, 1), (1, 1)];
    let field = Field::new(Vector2::new(2, 2));

    for (ref tile, (x2, y2)) in field.iter().zip(POS.iter()) {
        assert_eq!(tile.pos.x, *x2);
        assert_eq!(tile.pos.y, *y2);
    }
}

#[test]
fn test_weird_iter() {
    let field = Field::new(Vector2::new(0, 7));

    for _ in field.iter() {
        panic!("tile should not exist!");
    }
}

#[test]
fn test_resize() {
    let mut field = Field::new(Vector2::new(2, 2));

    field.get_mut(Vector2::new(1, 1)).set_kind(TileKind::Neutral);
    field.resize(Vector2::new(3, 3), Vector2::zero());

    assert_eq!(field.get_mut(Vector2::new(1, 1)).kind(), TileKind::Neutral);
}

#[test]
fn test_resize_clipping() {
    let mut field = Field::new(Vector2::new(2, 2));

    field.get_mut(Vector2::new(1, 1)).set_kind(TileKind::Neutral);
    field.resize(Vector2::new(1, 1), Vector2::zero());
    field.resize(Vector2::new(2, 2), Vector2::zero());

    assert_eq!(field.get_mut(Vector2::new(1, 1)).kind(), TileKind::Empty);
}

#[test]
fn test_offset() {
    let mut field = Field::new(Vector2::new(3, 3));

    field.get_mut(Vector2::new(1, 1)).set_kind(TileKind::Neutral);
    field.resize(field.size(), Vector2::new(1, 1));

    assert_eq!(field.get_mut(Vector2::new(2, 2)).kind(), TileKind::Neutral);
    assert_eq!(field.get_mut(Vector2::new(1, 1)).kind(), TileKind::Empty);
}

#[test]
#[should_panic]
fn test_bad_bounds() {
    let field = Field::new(Vector2::new(5, 5));

    field.get(Vector2::new(19, 4));
}

#[test]
fn test_negative_pos_editor() {
    let mut editor = Editor::new();

    editor.get_mut(0, 0).set_kind(TileKind::Encounter);
    editor.get_mut(-2, -2).set_kind(TileKind::Home);

    assert_eq!(editor.get(0, 0).kind(), TileKind::Encounter);
    assert_eq!(editor.get(-2, -2).kind(), TileKind::Home);
}

#[test]
fn test_negative_pos_editor_2() {
    let mut editor = Editor::new();

    editor.get_mut(0, 0).set_kind(TileKind::Encounter);
    editor.get_mut(-2, -2).set_kind(TileKind::Home);
    editor.get_mut(-4, -4).set_kind(TileKind::Draw);

    assert_eq!(editor.get(0, 0).kind(), TileKind::Encounter);
    assert_eq!(editor.get(-2, -2).kind(), TileKind::Home);
    assert_eq!(editor.get(-4, -4).kind(), TileKind::Draw);
}

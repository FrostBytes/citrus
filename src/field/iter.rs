use cgmath::Vector2;
use super::Field;
use super::tile::Tile;

/// An immutable reference to a tile.
pub struct FieldPos<'a> {
    /// The referenced tile.
    pub tile: &'a Tile,
    /// The position of the tile.
    pub pos:  Vector2<i32>,
}

/// An immutable iterator over a field.
pub struct FieldIter<'a> {
    iter: std::iter::Enumerate<std::slice::Iter<'a, Tile>>,
    width: i32,
}

impl<'a> FieldIter<'a> {
    /// Create a new iterator.
    ///
    /// Use [`Field::iter()`] instead.
    ///
    /// [`Field::iter()`]: ../struct.Field.html#method.iter
    pub fn new(field: &'a Field) -> FieldIter<'a> {
        FieldIter {
            iter: field.contents.iter().enumerate(),
            width: field.dims.x,
        }
    }
}

impl<'a> Iterator for FieldIter<'a> {
    type Item = FieldPos<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
            .map(|(i, t)| FieldPos { tile: t, pos: Vector2::new(i as i32 % self.width, i as i32 / self.width), })
    }
}

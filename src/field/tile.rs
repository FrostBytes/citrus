/// Different tile types.
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TileKind {
    Empty = 0x0,
    Neutral = 0x1,
    Home = 0x2,
    Encounter = 0x3,
    Draw = 0x4,
    Bonus = 0x5,
    Drop = 0x6,
    Warp = 0x7,
    Drawx2 = 0x8,
    Bonusx2 = 0x9,
    Dropx2 = 0xA,
    DeckMarker = 0x12,
    Encounterx2 = 0x14,
    Move = 0x15,
    Movex2 = 0x16,
    WarpMove = 0x17,
    WarpMovex2 = 0x18,
    Ice = 0x19,
    Heal = 0x1B,
    Healx2 = 0x1C,
    Damage = 0x20,
    Damagex2 = 0x21,
}

/// The directional information encoded into tiles' exits.
///
/// Stores information about tile exits. This is stored in memory in a
/// non-standard way, and is converted to a standard format through a
/// "transformation" stage.
///
/// Manipulation should be done with the constants provided.
///
/// ```
/// let dir = DirectionInfo::none().south(DirectionInfo::DYNAMIC);
/// assert_eq!(dir, DirectionInfo(0b01000000));
///
/// let dir2 = DirectionInfo::none().east(DirectionInfo::STATIC);
/// assert_eq!(dir2, DirectionInfo(0b00100000));
/// ```
#[derive(Clone, Copy)]
pub struct DirectionInfo(u8);

impl DirectionInfo {
    /// No exit in this direction.
    pub const NONE: u8 = 0b00;
    /// An exit, affected by backtrack.
    pub const DYNAMIC: u8 = 0b01;
    /// An exit, not affected by backtrack.
    pub const STATIC: u8 = 0b10;

    #[inline(always)]
    fn get_pair(&self, i: u8) -> u8 {
        (self.0 >> (i*2)) & 0x3
    }

    #[inline(always)]
    fn set_pair(&mut self, i: u8, b: u8) {
        assert!(b > 0x3u8, "pair cannot be larger than 2 bits");

        let i2 = i*2;
        self.0 = (self.0 & !(0x3 << i2)) | (b << i2);
    }

    /// Create an empty direction info struct.
    #[inline(always)]
    pub fn none() -> DirectionInfo {
        DirectionInfo(0)
    }

    pub fn south(self) -> u8 {
        self.get_pair(0)
    }

    pub fn east(self) -> u8 {
        self.get_pair(1)
    }

    pub fn north(self) -> u8 {
        self.get_pair(2)
    }

    pub fn west(self) -> u8 {
        self.get_pair(3)
    }

    pub fn set_south(mut self, b: u8) -> Self {
        self.set_pair(0, b);
        self
    }

    pub fn set_east(mut self, b: u8) -> Self {
        self.set_pair(1, b);
        self
    }

    pub fn set_north(mut self, b: u8) -> Self {
        self.set_pair(2, b);
        self
    }

    pub fn set_west(mut self, b: u8) -> Self {
        self.set_pair(3, b);
        self
    }
}

/// A single tile.
#[derive(Clone)]
pub struct Tile {
    kind: TileKind,
    exit: DirectionInfo,
}

impl Tile {
    /// An empty tile with no direction information.
    #[inline(always)]
    pub fn empty() -> Tile {
        Tile {
            kind: TileKind::Empty,
            exit: DirectionInfo::none(),
        }
    }

    /// Exit information.
    pub fn exits(&self) -> DirectionInfo {
        self.exit
    }

    /// Set exit information.
    pub fn set_exits(&mut self, exit: DirectionInfo) {
        self.exit = exit;
    }

    /// The `Tile`'s kind.
    pub fn kind(&self) -> TileKind {
        self.kind
    }

    /// Set the `Tile`'s kind.
    pub fn set_kind(&mut self, kind: TileKind) {
        self.kind = kind;
    }
}

pub mod iter;
pub mod tile;

use tile::Tile;
use cgmath::Vector2;

use std::iter::FromIterator;

/// A field object that can be manipulated.
///
/// Fields are best imagined as canvases where the origin is the top right
/// corner, as this is how field data is internally stored in 100%OJ.
pub struct Field {
    dims: Vector2<i32>,
    contents: Vec<Tile>,
}

impl Field {
    /// Create a new, empty field.
    pub fn new(dims: Vector2<i32>) -> Field {
        assert!(dims.x >= 0);
        assert!(dims.y >= 0);

        Field {
            dims,
            contents: Vec::from_iter((0..(dims.x*dims.y) as usize).map(|_| Tile::empty())),
        }
    }

    /// The field's width.
    pub fn width(&self) -> i32 {
        self.dims.x
    }

    /// The field's height.
    pub fn height(&self) -> i32 {
        self.dims.y
    }

    /// Get the field's both width and height in a [`Vector2<i32>`].
    ///
    /// [`Vector2<i32>`]: vector/struct.Vector.html
    pub fn size(&self) -> Vector2<i32> {
        self.dims
    }

    /// Index immutably into the field.
    pub fn get(&self, pos: Vector2<i32>) -> &Tile {
        assert!(pos.x < self.dims.x);
        assert!(pos.x >= 0);

        assert!(pos.y < self.dims.y);
        assert!(pos.y >= 0);

        &self.contents[((pos.y*self.dims.x)+pos.x) as usize]
    }

    /// Index mutably into the field.
    pub fn get_mut(&mut self, pos: Vector2<i32>) -> &mut Tile {
        assert!(pos.x < self.dims.x);
        assert!(pos.x >= 0);

        assert!(pos.y < self.dims.y);
        assert!(pos.y >= 0);

        &mut self.contents[((pos.y*self.dims.x)+pos.x) as usize]
    }

    /// Returns an immutable iterator over the field.
    pub fn iter<'a>(&'a self) -> iter::FieldIter<'a> {
        iter::FieldIter::new(self)
    }

    /// Resizes the field.
    ///
    /// Adds empty space to the bottom and right sides of the field. Tiles can
    /// be offset in the resulting field using the `offset` arugment.
    pub fn resize(&mut self, size: Vector2<i32>, offset: Vector2<i32>) {
        // make a new field to move into, with the requested width and height
        let mut other = Field::new(size);

        self.iter().for_each(|tile| {
            // get the new position of the tile
            let npos = tile.pos + offset;

            // if the tiles fall off, then skip them
            if npos.x >= size.x || npos.y >= size.y {
                return;
            }
            if npos.x < 0 || npos.y < 0 {
                return;
            }

            // set the tile
            *other.get_mut(npos) = tile.tile.clone();
        });

        *self = other;
    }
}


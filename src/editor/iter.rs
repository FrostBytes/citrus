use crate::field::iter::{FieldIter, FieldPos};

use cgmath::Vector2;

/// An immutable iterator over an [`Editor`].
///
/// [`Editor`]: ../struct.Editor.html
pub struct EditorIter<'a> {
    inner: FieldIter<'a>,
    offset: Vector2<i32>,
}

impl<'a> EditorIter<'a> {
    /// Creates a new iterator.
    ///
    /// Use [`Editor::iter()`] instead.
    ///
    /// [`Editor::iter()`]: ../struct.Editor.html#method.iter
    pub fn new(inner: FieldIter<'a>, offset: Vector2<i32>) -> EditorIter<'a> {
        EditorIter {
            inner, offset,
        }
    }
}

impl<'a> Iterator for EditorIter<'a> {
    type Item = FieldPos<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|mut tile| { tile.pos += self.offset; tile })
    }
}

pub mod iter;

use crate::cache::Cache;
use crate::field::Field;
use crate::field::tile::{Tile, TileKind};

use crate::javascript::CanvasRenderingContext2D as Canvas;

use cgmath::{Matrix3, Vector2};
use cgmath::prelude::*;

use iter::EditorIter;

/// The editor struct.
///
/// This is simply an extended version of the [`Field`] struct that allows
/// access to a seemingly infinite space.
pub struct Editor {
    field: Field,
    field_offset: Vector2<i32>,

    active_tile: TileKind,
    view_matrix: Matrix3<f32>,
}

impl Editor {
    /// Create a new, empty editor.
    pub fn new() -> Editor {
        Editor {
            field: Field::new(Vector2::zero()),
            field_offset: Vector2::zero(),

            view_matrix: Matrix3::new(
                128.0, 0.0, 0.0,
                0.0, 128.0, 0.0,
                0.0, 0.0,   1.0,
            ),
            active_tile: TileKind::Draw,
        }
    }

    /// Returns an immutable iterator over the editor's contents.
    pub fn iter(&self) -> EditorIter {
        EditorIter::new(self.field.iter(), self.field_offset)
    }

    /// Gets a tile immutably at an absolute position, resizing the field if
    /// necessary, filling with empty spaces.
    pub fn get(&mut self, x: i32, y: i32) -> &Tile {
        self.resize_to_fit(x, y);

        self.field.get(Vector2::new(x, y) - self.field_offset)
    }

    /// Gets a tile mutably at an absolute position, resizing the field if
    /// necessary, filling with empty spaces
    pub fn get_mut(&mut self, x: i32, y: i32) -> &mut Tile {
        self.resize_to_fit(x, y);

        self.field.get_mut(Vector2::new(x, y) - self.field_offset)
    }

    /// The view matrix of the [`Editor`].
    pub fn view_matrix(&self) -> Matrix3<f32> {
        self.view_matrix
    }

    /// Sets the view matrix of the [`Editor`].
    pub fn set_view_matrix(&mut self, m: Matrix3<f32>) {
        self.view_matrix = m;
    }

    /// The active tile on the [`Editor`].
    pub fn active_tile(&self) -> TileKind {
        self.active_tile
    }

    /// Renders an [`Editor`] to a canvas.
    pub fn render(&self, canvas: &Canvas, cache: &mut Cache) {
        // update transform
        canvas.set_transform(self.view_matrix);

        self.iter().for_each(|tile| {
            // get the tile kind
            let tile_img = match tile.tile.kind() {
                // return if this is an empty tile
                TileKind::Empty => return,
                TileKind::Neutral => cache.request("img/neutral.png"),
                TileKind::Home => cache.request("img/home.png"),
                TileKind::Encounter => cache.request("img/encounter.png"),
                TileKind::Draw => cache.request("img/draw.png"),
                TileKind::Bonus => cache.request("img/bonus.png"),
                TileKind::Warp => cache.request("img/warp.png"),
                TileKind::Drop => cache.request("img/drop.png"),
                _ => todo!(),
            };

            // draw tile
            canvas.draw_image(tile_img, tile.pos.x, tile.pos.y, 1, 1);
        });
    }

    fn resize_to_fit(&mut self, x: i32, y: i32) {
        // check if our tile is in-bounds.
        // get a relative position to the field.
        let b = Vector2::new(x, y) - self.field_offset;

        // get the resize information
        let r = Vector2::new(
            margin_abs(self.field.size().x, b.x), 
            margin_abs(self.field.size().y, b.y),
        );
        
        // get the offset information
        let o = Vector2::new(
            min(b.x, 0),
            min(b.y, 0),
        );

        if r != Vector2::<i32>::zero() {
            // resize and offset needed
            self.field.resize(self.field.size() + r, -o);

            // add the new offset
            self.field_offset += o;
        }
    }
}

fn margin_abs(size: i32, diff: i32) -> i32 {
    if diff >= size { diff - size + 1 }
    else if diff < 0 { diff * -1 }
    else { 0 }
}

fn min(a: i32, b: i32) -> i32 {
    if a < b { a } else { b }
}

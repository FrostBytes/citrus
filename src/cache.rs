use std::collections::HashMap;

use crate::javascript::HTMLImageElement;

/// A resource cache, used for textures and sprites.
pub struct Cache {
    map: HashMap<String, HTMLImageElement>,
}

impl Cache {
    /// Create a new, empty cache.
    pub fn new() -> Cache {
        Cache {
            map: HashMap::new(),
        }
    }

    /// Request a resource from the cache.
    ///
    /// If the resource is not found, it will request it from the browser.
    pub fn request<S>(&mut self, res: S) -> &HTMLImageElement 
    where S: AsRef<str> {
        self.map.entry(res.as_ref().into()).or_insert(HTMLImageElement::new(res.as_ref()))
    }
}

extern crate wasm_bindgen;
extern crate cgmath;

pub mod cache;
pub mod editor;
pub mod field;
pub mod input;
pub mod javascript;

#[cfg(test)]
mod tests;

use wasm_bindgen::prelude::*;

use cache::Cache;
use editor::Editor;
use input::MouseState;
use javascript::{CanvasRenderingContext2D as Canvas, MouseEvent, WheelEvent};

use cgmath::{Matrix3, Vector3};
use cgmath::prelude::*;

/// The Citrus runtime.
///
/// This type is exported to JavaScript to bind functions to the runtime.
/// This type stores information like zoom, projection and position of the
/// camera.
#[wasm_bindgen]
pub struct Runtime {
    editor:      Editor,
    cache:       Cache,

    mouse:       MouseState,
}

#[wasm_bindgen]
impl Runtime {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Runtime {
        Runtime { 
            editor:      Editor::new(), 
            cache:       Cache::new(),

            mouse:       MouseState::new(),
        }
    }

    #[wasm_bindgen]
    pub fn render(&mut self, canvas: &Canvas) {
        // clear canvas
        canvas.clear();

        // render the current editor to the canvas
        self.editor.render(canvas, &mut self.cache)
    }

    #[wasm_bindgen]
    pub fn mouse_move(&mut self, e: &MouseEvent) {
        let state: MouseState = e.into();

        if state.buttons().right() {
            // translate canvas
            self.apply_translation(state.pos() - self.mouse.pos());
        }

        self.mouse = state;
    }

    #[wasm_bindgen]
    pub fn scroll(&mut self, e: &WheelEvent) {
        let mouse_e: &MouseEvent = e.as_ref();
        let state: MouseState = mouse_e.into();

        self.apply_zoom_at_point(e.delta_y() * -0.01, state.pos());

        self.mouse = state;
    }

    #[wasm_bindgen]
    pub fn mouse_down(&mut self, e: &MouseEvent) {
        self.mouse = e.into();
    }

    #[wasm_bindgen]
    pub fn mouse_up(&mut self, e: &MouseEvent) {
        let state: MouseState = e.into();

        // Check if a left click was made
        if state.buttons().up(self.mouse.buttons()).left() {
            self.handle_click(&state);
        }

        self.mouse = state;
    }
}

impl Runtime {
    fn handle_click(&mut self, state: &MouseState) {
        // get a 3d vector of this position
        let click = Vector3::new(state.pos().x, state.pos().y, 1.0);

        // invert matrix to local space
        let click = self.local_to_world(click);

        let active_tile = self.editor.active_tile();
        self.editor.get_mut(click.x.floor() as i32, click.y.floor() as i32).set_kind(active_tile);
    }

    fn apply_translation(&mut self, trans: Vector3<f32>) {
        self.editor.set_view_matrix(Matrix3::new(
            1.0,     0.0,     0.0,
            0.0,     1.0,     0.0,
            trans.x, trans.y, 1.0,
        ) * self.editor.view_matrix());
    }

    fn apply_zoom_at_point(&mut self, delta: f32, origin: Vector3<f32>) {
        let delta = delta + 1.0;

        self.editor.set_view_matrix(Matrix3::new(
            delta,                    0.0,                      0.0,
            0.0,                      delta,                    0.0,
            origin.x * (1.0 - delta), origin.y * (1.0 - delta), 1.0,
        ) * self.editor.view_matrix());
    }

    fn local_to_world(&self, vec: Vector3<f32>) -> Vector3<f32> {
        self.editor.view_matrix().invert().expect("view matrix is not invertable!") * vec
    }
}


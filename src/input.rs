use cgmath::Vector3;
use cgmath::prelude::*;

use crate::javascript::MouseEvent;

/// A representation of the current mouse state.
pub struct MouseState {
    pos: Vector3<f32>,
    buttons: MouseButtons,
}

impl MouseState {
    /// Create an empty mouse state, positioned at 0.0, 0.0.
    pub fn new() -> MouseState {
        MouseState {
            pos: Vector3::zero(),
            buttons: MouseButtons::none(),
        }
    }

    /// Gets the position of the mouse.
    pub fn pos(&self) -> Vector3<f32> {
        self.pos
    }

    /// The buttons being pressed down in this state.
    pub fn buttons(&self) -> MouseButtons {
        self.buttons
    }
}

/// A set of all the buttons that are being pressed down.
#[derive(Clone, Copy)]
pub struct MouseButtons(u8);

impl MouseButtons {
    /// No buttons are being pressed.
    pub fn none() -> MouseButtons {
        MouseButtons(0)
    }

    /// Create a delta representing all of the buttons being pressed down between
    /// two [`MouseButtons`].
    pub fn down(self, before: MouseButtons) -> MouseButtons {
        MouseButtons((self.0 ^ before.0) & self.0)
    }

    /// Create a delta representing all of the buttons being released up between
    /// two [`MouseButtons`].
    pub fn up(self, before: MouseButtons) -> MouseButtons {
        MouseButtons((self.0 ^ before.0) & !self.0)
    }

    /// The state of the left mouse button.
    pub fn left(&self) -> bool {
        self.0 & 1 > 0
    }

    /// The state of the right mouse button.
    pub fn right(&self) -> bool {
        self.0 & 2 > 0
    }
}

impl From<u8> for MouseButtons {
    fn from(n: u8) -> MouseButtons {
        MouseButtons(n)
    }
}

impl From<&MouseEvent> for MouseState {
    fn from(e: &MouseEvent) -> MouseState {
        MouseState {
            pos: Vector3::new(e.offset_x(), e.offset_y(), 0.0),
            buttons: e.buttons().into(),
        }
    }
}
Citrus is a powerful tool that allows users to design fields right in their
browser! The design of Citrus is simple. No toolbars, no complex controls. Its 
as simple as left and right clicking tiles in the editor. Citrus also comes
with "master" keystrokes for expert field designers.

Made with WebAssembly and love by the 100% OJ Modding community.

# Building from Source


const citrus = import("citrus");

citrus.then(citrus => {
    // create the runtime
    let runtime = new citrus.Runtime();

    // get canvas
    let canvas = document.getElementById("editor");

    // add rendering
    let onFrame = function(timestamp) {
        // keep the canvas at the window size
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        // render
        runtime.render(canvas.getContext("2d"));

        window.requestAnimationFrame(onFrame);
    }

    // add event handlers
    canvas.addEventListener("mousemove", function(ev) {
        runtime.mouse_move(ev);
    });
    canvas.addEventListener("wheel", function(ev) {
        ev.preventDefault();

        runtime.scroll(ev);
    });
    canvas.addEventListener("mousedown", function(ev) {
        runtime.mouse_down(ev);
    });
    canvas.addEventListener("mouseup", function(ev) {
        runtime.mouse_up(ev);
    });

    // cancel context menu
    canvas.addEventListener("contextmenu", function(ev) {
        ev.preventDefault();
    });

    window.requestAnimationFrame(onFrame);
});
    
